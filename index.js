var express = require('express');
var fs = require('fs');
var app = express();

try{
  fs.readFile('./server.conf', function(err, data){
    run(JSON.parse(data));
  });
}
catch(e){
  console.error("Server could not be started: Configuration file unreadable.");
  return;
}

function run(config){

  if(!config || !config.port){
    console.error("Server could not be started: Configuration file missing data.");
    return 0;
  }

  app.use(express.static('./'));

  app.get('/', function (req, res) {
    res.sendFile('html/index.html', { root: __dirname });
  })

  app.get('/index.html', function(req, res){
    res.redirect('/');
  })

  app.get('/people.html', function (req, res) {
    res.sendFile('html/people.html', { root: __dirname });
  })
  
  app.get('/resources.html', function (req, res) {
    res.sendFile('html/resources.html', { root: __dirname });
  })

  app.get('/contact.html', function (req, res) {
    res.sendFile('html/contact.html', { root: __dirname });
  })

  app.get('/donate.html', function (req, res) {
    res.sendFile('html/donate.html', { root: __dirname });
  })
  
  app.get('/brochure', function (req, res) {
    res.sendFile('resources/FHTL_Brochure.pdf', { root: __dirname });
  })
  
  app.get('/sampler', function (req, res) {
    res.sendFile('resources/FHTL_Booklet.pdf', { root: __dirname });
  })

  app.listen(config.port, function () {
    console.log('Example app listening on port', config.port + '!')
  })

}
var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope,$window,$interval) {
	$scope.smallScreen = false;
	$scope.displayModal = false;
	$scope.currentIcon = 0;
	var interval = $interval(function () {
  			$scope.currentIcon = ($scope.currentIcon + 1) * ($scope.currentIcon < 15);
  			$scope.setSlide($scope.currentIcon);
  			
  			//Uncomment this portion to allow the pictures to scroll as they are being cycled through
  			/*var element = document.getElementById("peopleBox");
  			var width = 0;
            if (window.innerWidth > 600) {
                width = 200;
            }
            else {
                width = 100;
            }
            
            element.scrollLeft = width * ($scope.currentIcon);*/

		}, 8000);
	$scope.cancelInterval = function() {
		$interval.cancel(interval);
	}

	$scope.personList = [
		{name:'Dr. Sederberg',description:'Tom Sederberg is a professor of computer science and associate dean of the BYU College of Physical and Mathematical Sciences. He and Dr. Barrett founded the Family History Technology Lab in about 1998.',image:'sederberg.jpg'},
		{name:'Dr. Barrett',description:'Dr. Barrett is a professor of computer science at Brigham Young University where he has served as Department Chair and Associate Chair. He and Dr. Sedeberg founded the Family History Technology as part of a larger effort to expand technology\'s role in simplifying and automating family history.',image:'woodfield.jpg'},
		{name:'Dr. Clement',description:'Dr. Clement is a professor of computer science at Brigham Young University. His research at BYU centers on high-performance computing and bioinformatics.',image:'clement.png'},
		{name:'Jordan',description:'Jordan is originally from Orlando, Florida and is here at BYU working towards his Bachelors in Computer Science, with a minor in Business Management. He joined the FHTL team in January 2017, and is currently working on RelativeFinder. Outside of classes and work he enjoys playing tennis, spending time with family, and reading.',image:'sederberg.jpg'},
		{name:'Calvin',description:'Calvin is from Logandale, Nevada and is a Junior majoring in Computer Science. Since January, 2016, Calvin has helped rewrite One Page Genealogy, and is now developing new apps that will be released in coming months. In his spare time, Calvin enjoys watching movies and ice cream with his wife, Emmalee.',image:'barrett.jpg'},
		{name:'Jeremy',description:'Jeremy Hodges is a BYU student studying computer science and mathematics. In his free time he likes to spend time with his wife, go running, and play basketball.',image:'clement.png'},
		{name:'Joseph',description:'Joseph is from Layton, Utah, but has lived in a lot of cool places including England and Spain! He served his mission in Bogota, Colombia. He is a CS Major who just finished his freshman year at BYU and has been working at the FHTL lab since February 2017. He is currently working on Virtual Pedigree and One Page Geneology, and in his free time he likes reading, sports and spending time with family and friends.',image:'sederberg.jpg'},
		{name:'Jacob',description:'Jacob is from Mountain View, Wyoming. He is studying Spanish Teaching and working on a minor in Computer Science. In his spare time, he enjoys web design, spending time with family, and watching movies. He is currently working on Pedigree Pie and Geneopardy for the FHTL team.',image:'woodfield.jpg'},
		{name:'Wyatt',description:'Wyatt is a Computer Science student at BYU. He grew up in Springville, Ut. He joined the FHTL in May of 2017. He has dreams of someday being able to hack things forward and backward in time. Wyatt also enjoys all things outdoors including activities such as rock climbing, mountain biking, and hiking.',image:'clement.png'},
		{name:'Mike',description:'Mike is from West Valley City, Utah. He\'s studying two majors - Family History and Computer Science. He enjoys doing genealogy, running, and spending time with friends. It’s his job to respond to user support emails and resolve issues with Relative Finder accounts.',image:'sederberg.jpg'},
		{name:'Nate',description:'Nate is from Muncie, Indiana, and has worked in the lab since February of 2016. He is currently developing DescendancyExplorer, a new app that will be released in coming months. Outside the lab, Nate enjoys camping, hiking, woodworking, and baking.',image:'barrett.jpg'},
		{name:'Chris',description:'Chris is a Junior at BYU majoring in Computer Science and minoring in Scandinavian Studies. He started working in the Family History lab in May 2017. Chris is originally from Maryland, just outside of Washington DC, and loves to play, write, and record music in his free time.',image:'clement.png'},
		{name:'Spencer',description:'Spencer is from Holladay, Utah. He is studying Economics with minors in Computer Science and Business Strategy. He enjoys playing the guitar and listening to music.',image:'sederberg.jpg'},
		{name:'Jared',description:'Jared is from Rescue, California and is majoring in Computer Engineering. Outside of classes and work he enjoys running, backpacking and playing disc golf.',image:'woodfield.jpg'},
		{name:'Ryan',description:'Ryan is a CS major from Southern California. He enjoys competing in food challenges, playing video games, and power lifting. When he isn\'t playing sports or coding he is often found napping or watching YouTube.',image:'clement.png'},
		{name:'Ben',description:'Ben is from Holladay, Utah. He is studying Computer Science with a minor in Business Management. In his free time, he enjoys being outdoors, reading and spending time with friends.',image:'sederberg.jpg'}
	];
					   
	$scope.setSlide = function($index) {
		$scope.currentIcon = $index;
	}
});

app.directive('myDirective', ['$window', function ($window) {
 return {
    link: link,
    restrict: 'A'           
 };
 function link(scope, element, attrs){
    scope.width = $window.innerWidth;
    
        function onResize(){
            console.log($window.innerWidth);
            // uncomment for only fire when $window.innerWidth change   
            if (scope.width !== $window.innerWidth)
            {
                scope.width = $window.innerWidth;
                scope.$digest();
            }
        };

        function cleanUp() {
            angular.element($window).off('resize', onResize);
        }

        angular.element($window).on('resize', onResize);
        scope.$on('$destroy', cleanUp);
 }



}]);
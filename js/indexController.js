	var app = angular.module('myApp', []);

	app.controller('myCtrl', function($scope,$window,$interval) {
		$scope.smallScreen = false;
		$scope.test = function() {
			console.log("TEST");
		}
		$scope.displayModal = false;
		$scope.currentIcon = 0;
		var interval = $interval(function () {
  			$scope.currentIcon = ($scope.currentIcon + 1) * ($scope.currentIcon < 7);
  			$scope.setSlide($scope.currentIcon);
  			console.log($scope.currentIcon);
			}, 8000);
		$scope.cancelInterval = function() {
			console.log("CANCEL");
			$interval.cancel(interval);
		}

		$scope.iconList = [
						   {name:'Home',color:'linear-gradient(45deg, #00b2ea, #0097de 17%, #017dca 31%, #0459b1 73%, #1142a5)'},
		                   {name:'Relative Finder',color:'linear-gradient(45deg, #2f7675, #255c5c)'},
						   {name:'Virtual Pedigree',color:'linear-gradient(45deg, #135369, #273646)'},
						   {name:'One Page Geneology',color:'linear-gradient(180deg, #273646, #273646)'},
						   {name:'Descendancy Explorer',color:'linear-gradient(45deg, #0c60ee, #7ca0ff)'},
						   {name:'Pedigree Pie',color:'linear-gradient(45deg, #5f3f20, #835f3d 57%)'},
						   {name:'Geneopardy',color:'linear-gradient(45deg, #00b2ea, #0097de 17%, #017dca 31%, #0459b1 73%, #1142a5)'},
						   {name:'Wheel of Family Fortune',color:'linear-gradient(45deg, #2d3288, #464a8e)'}
						  ];

		$scope.headerList = [
						   {name:'Welcome to the Family History Technology Lab',description:'The BYU Family History Technology Lab is a non-profit student research lab sponsored by the BYU Computer Science department. We\'re dedicated to lowering the entry barrier into family history work by providing simple, usable, and enjoyable family history apps.',url:'#'},
		                   {name:'Relative Finder',description:'Ever wonder if you\'ve got some long-lost cousins out there? Relative Finder allows you to see how you are related to friends, coworkers, prophets, historical figures, and more! Use our community features to connect with your relatives from around the globe!',url:'https://www.relativefinder.org/#/main'},
						   {name:'Virtual Pedigree',description:'Virtual Pedigree allows you to navigate your genealogy with a new and revolutionary fluid interface. Simply click (or touch!) and drag, and begin exploring! It gives you hints and help as you explore your tree, and now includes LDS Ordinance information. Take it for a spin!',url:'https://virtual-pedigree.fhtl.byu.edu/login/'},
						   {name:'One Page Geneology',description:'One Page Genealogy is the place for you to view, customize, and download your family tree all on one page! View up to 20 generations of ascendancy or descendancy. Change colors and tree styles. Download and print to show your family and friends!',url:'https://opg.fhtl.byu.edu/'},
						   {name:'Descendancy Explorer',description:'Let the computer find names ready to take to the temple with the click of a button! Descendancy Explorer searches your family tree for temple ordinances you can do right now. Download or filter the results and then it\'s temple time!',url:'https://descend.fhtl.byu.edu/#/login'},
						   {name:'Pedigree Pie',description:'25% American. 75% Irish. 100% You. Pedigree Pie shows your international heritage in a single easy-to-read chart. Click on the chart to view details about where and when your ancestors lived, and to trace back your family\'s path through history!',url:'https://pedigree-pie.fhtl.byu.edu/'},
						   {name:'Geneopardy',description:'What is a game that tests your knowledge of your family tree? Geneopardy! If you think you know your family tree, try out this fun game and prove your knowledge to your friends and families. Enjoy hours of entertainment at family reunions or just on your own.',url:'https://geneopardy.fhtl.byu.edu/'},
						   {name:'Wheel of Family Fortune',description:'It pays to know your family history. Wheel of Family Fortune is the classic party game that tests how well you know your family history. Play alone, or add other players to create a fun game for the whole family!',url:'https://wheel.fhtl.byu.edu/#/'}
						  ];
						   
		$scope.setSlide = function($index) {
			$scope.currentIcon = $index;
			document.body.style.backgroundImage = $scope.iconList[$index].color;
			document.body.style.backgroundRepeat = "no-repeat";
			document.body.style.backgroundSize = "100% 250%";
		}
	});

	app.directive('myDirective', ['$window', function ($window) {
 return {
    link: link,
    restrict: 'A'           
 };
 function link(scope, element, attrs){
    scope.width = $window.innerWidth;
    
        function onResize(){
            console.log($window.innerWidth);
            // uncomment for only fire when $window.innerWidth change   
            if (scope.width !== $window.innerWidth)
            {
                scope.width = $window.innerWidth;
                scope.$digest();
            }
        };

        function cleanUp() {
            angular.element($window).off('resize', onResize);
        }

        angular.element($window).on('resize', onResize);
        scope.$on('$destroy', cleanUp);
 }



}]);